
public class StudentsList {

	Student students[];
	Integer size;

	public StudentsList(){
		size = 0;
	}

	public void register(String code, String name, Integer grade){
		Student[] _buffStudents;
		_buffStudents = new Student[size];
		for(Integer i=0;i<size;i++){
			_buffStudents[i] = students[i];
		}
		students = new Student[size+1];
		for(Integer i=0;i<size;i++){
			students[i] = _buffStudents[i];
		}
		students[size] = new Student(code,name,grade);
		size++;
	}

	public void sort(){
		
	}

	public int size(){
		return size;
	}

	public void show(){
		for(Integer i=0;i<size;i++){
			students[i].show();
		}
	}

}
