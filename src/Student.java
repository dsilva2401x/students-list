
public class Student {

	String code;
	String name;
	Integer grade;

	public Student(String _code,String _name, Integer _grade) {
		code = _code;
		name = _name;
		grade = _grade;
	}

	public String getCode(){
		return code;
	}
	public String getName(){
		return name;
	}
	public Integer getGrade(){
		return grade;
	}

	public void show(){
		System.out.println("Code: "+code+"\nName: "+name+"\nGrade: "+grade+"\n");
	}
}
